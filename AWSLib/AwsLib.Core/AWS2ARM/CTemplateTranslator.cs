﻿/***********************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<http://www.gnu.org/licenses/>.
***********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwsLib.Core.AzureResourceManager;

namespace AwsLib.Core.AWS2ARM
{
    public class CTemplateTranslator
    {
        awsLib.awsObject _AWSTemplate;
        ArmTemplate _ARMTemplate;
        public CTemplateTranslator(awsLib.awsObject theAWSTemplate)
        {
            _AWSTemplate = theAWSTemplate;
        }
        public ArmTemplate Translate()
        {
            // Create the target ARM template
            _ARMTemplate = new ArmTemplate();

            // Copy version
            _ARMTemplate.ArmTemplateDefinition.ContentVersion = _AWSTemplate.Version;

            // Translating all parameters
            foreach (awsLib.IAWSParameter AWSParam in _AWSTemplate.Parameters)
            {
                AWS2ARM.CParameterTranslator PT;
                // Look for the custom parameter translator if there is one. Use Default translator otherwise
                if (AWS2ARM.CParameterTranslatorInfo.AllParameterTranslators.ContainsKey(AWSParam.Name))
                {
                    // Retrieve appropriate translator infos for the parameter
                    AWS2ARM.CParameterTranslatorInfo PTI = AWS2ARM.CParameterTranslatorInfo.AllParameterTranslators[AWSParam.Name];

                    // Create and get the custom translator for the param type
                    Type T = Type.GetType(PTI.ParamTranslatorClass);
                    var TO = Activator.CreateInstance(T,new object[] { AWSParam });
                    PT = (CParameterTranslator)TO;
                }
                else
                {
                    PT = new CDefaultParameterTranslator(AWSParam);
                }
                // Translate the parameter and add it to the ARM template
                _ARMTemplate.ArmTemplateDefinition.Parameters.Add(PT.Translate());
            }

            // Translating resources of the template
            foreach (var AWSResource in _AWSTemplate.Resources)
            {
                // Look for the custom Resource translator if there is one. Use Default translator otherwise
                AWS2ARM.CResourceTranslator RT;
                if (CResourceTypeTranslatorInfo.AllResourceTypeTranslators.ContainsKey(AWSResource.Name))
                {
                    AWS2ARM.CResourceTypeTranslatorInfo RTTI = AWS2ARM.CResourceTypeTranslatorInfo.AllResourceTypeTranslators[AWSResource.Name];
                    Type T = Type.GetType(RTTI.ResourceTypeTranslatorClass);
                    var TO = Activator.CreateInstance(T, new object[] { AWSResource });
                    RT = (CResourceTranslator)TO;
                    // Translate the parameter and add it to the ARM template
                    _ARMTemplate.ArmTemplateDefinition.Resources.Add(RT.Translate());
                }
                else
                {
                    RT = null;
                }
            }

            return _ARMTemplate;
        }
    }
}



