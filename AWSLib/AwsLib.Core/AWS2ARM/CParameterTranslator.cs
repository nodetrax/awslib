﻿/***********************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<http://www.gnu.org/licenses/>.
***********************************************************************/


using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using awsLib;
using AwsLib.Core.AzureResourceManager;

namespace AwsLib.Core.AWS2ARM
{
    abstract class CParameterTranslator
    {
        protected IAWSParameter _awsParameter;
        public CParameterTranslator(IAWSParameter awsParameter)
        {
            _awsParameter = awsParameter;
        }
        public abstract ArmParameter Translate();
    }

    // Default parameter translator will translate param name 
    // and copy type, default value, allowed values 
    // Allowed pattern, Min Length and MAxLength are ingnored
    class CDefaultParameterTranslator : CParameterTranslator
    {
        public CDefaultParameterTranslator(IAWSParameter awsParameter) : base(awsParameter)
        {
        }

        public override ArmParameter Translate()
        {
            ArmParameter result = new ArmParameter();

            if(CParameterTranslatorInfo.AllParameterTranslators.ContainsKey(_awsParameter.Name))
            {
                result.Name = CParameterTranslatorInfo.AllParameterTranslators[_awsParameter.Name].AzureParamName;
            } else
            {
                result.Name = _awsParameter.Name;
            }
            result.Description = _awsParameter.Description;
            result.Type = _awsParameter.Type;
            result.DefaultValue = _awsParameter.Default;
            if (_awsParameter.AllowedValues != null) {
                foreach(string s in _awsParameter.AllowedValues)
                {
                    result.AllowedValues.Add(s);
                }
            }
            // Ignoring other param attributes
            //_awsParameter.AllowedPattern
            //_awsParameter.MaxLength 
            //_awsParameter.MinLength

            return result;
        }
    }




    public class CParameterTranslatorInfo
    {
        public string AWSParamName;
        public string AzureParamName;
        public string ParamTranslatorClass;

        static SortedDictionary<String, AWS2ARM.CParameterTranslatorInfo> _AllParameterTranslators = new SortedDictionary<String, AWS2ARM.CParameterTranslatorInfo>();
        public static SortedDictionary<String,AWS2ARM.CParameterTranslatorInfo> AllParameterTranslators { get { return _AllParameterTranslators; } }
        public static void LoadAllParametersInfo(string JSONFile)
        {
            _AllParameterTranslators = JsonConvert.DeserializeObject<SortedDictionary<String, AWS2ARM.CParameterTranslatorInfo>>(File.ReadAllText(JSONFile));
        }

    }
}
