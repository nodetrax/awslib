﻿/***********************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<http://www.gnu.org/licenses/>.
***********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsLib.Core.AWS2ARM
{
    class TranslateUtil
    {

        private const string PARAM_REF_PREFIX = "&";
        private const string RESOURCE_REF_PREFIX = "!";

        private static bool IsParamRef(string s)
        {
            return s.StartsWith(PARAM_REF_PREFIX);
        }
        private static bool IsResourceRef(string s)
        {
            return s.StartsWith(RESOURCE_REF_PREFIX);
        }

        private static string Paramify(string s)
        {
            return "[Parameters('" + s.Remove(0, PARAM_REF_PREFIX.Length) + "')]";
        }
        private static string Resourcify(string s)
        {
            return "RESOURCE:" + s.Remove(0,RESOURCE_REF_PREFIX.Length);
        }

        // Takes a string, detects whether it includes a param or resource reference
        // return the Azure reference if it is the case or the original string otherwise
        public static string ValueOrRef(string s)
        {
            if (IsParamRef(s))
            {
                return Paramify(s);
            }
            else
            {
                if (IsResourceRef(s))
                {
                    return Resourcify(s);
                } else
                {
                    return s;
                }
            }
        }

    }
}
