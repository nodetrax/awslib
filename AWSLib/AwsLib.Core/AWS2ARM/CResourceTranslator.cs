﻿/***********************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<http://www.gnu.org/licenses/>.
***********************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwsLib.Core.AzureResourceManager;
using AwsLib.Core.AzureResourceManager.ArmProperties;
using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.Network.Fluent.Models;

namespace AwsLib.Core.AWS2ARM
{

    abstract class CResourceTranslator
    {
        protected awsLib.awsResource _awsResource;
        public CResourceTranslator(awsLib.awsResource awsResource)
        {
            _awsResource = awsResource;
        }
        public abstract IArmResource Translate();

        // To be reimplmented when going to generic approach of ressources and properties
        public void DoProperties()
        {
            /*
            foreach(awsLib.IAWSResourceProperty awsProperty in _awsResource.Property.²)
            {
                CResourceTypeTranslatorInfo RTTY = CResourceTypeTranslatorInfo.AllResourceTypeTranslators[_awsResource.Name];
                CResourcePropertiesTranslatorInfo RPTI = RTTY.Properties[awsProperty.ToString()];

                CPropertyTranslator PT;
                var TO = Activator.CreateInstance("AwsLib.Core.AWS2ARM", RPTI.TranslatorClass);
                PT = (CPropertyTranslator)TO.Unwrap();

                //PT.Translate();
            }
            */
        }
    }


    class CAWSEC2InstanceTranslator : CResourceTranslator
    {
        public CAWSEC2InstanceTranslator(awsLib.awsResource awsResource) : base(awsResource)
        {
        }

        public override IArmResource Translate()
        {
            // Gert the resource properties and cast the to instance properties        
            awsLib.awsInstanceProperties awsInstanceProperties = (awsLib.awsInstanceProperties)_awsResource.Property;

            //Create the target ARM resource
            IArmResource R = ArmResourceFactory.CreateArmResourceByType(ArmResourceType.VirtualMachine);
            VirtualMachineInner VMI = (VirtualMachineInner)R.ResourceDefinition;

            //awsInstanceProperties.ImageId;
            //awsInstanceProperties.InstanceType;
            //awsInstanceProperties.KeyName;
            //awsInstanceProperties.SecurityGroups;
            //awsInstanceProperties.UserData


            var hardwareProfileProperty = new HardwareProfileProperty();
            hardwareProfileProperty.PropertyDefinition.VmSize = "Standard_F1";
            R.AddPropertyToResource(hardwareProfileProperty);

            var osProfileProperty = new OSProfileProperty();
            //osProfileProperty.PropertyDefinition.AdminUsername = awsInstanceProperties.;
            //osProfileProperty.PropertyDefinition.AdminPassword = awsInstanceProperties.KeyName;
            //osProfileProperty.PropertyDefinition.ComputerName = "vm1";
            osProfileProperty.PropertyDefinition.CustomData = awsInstanceProperties.UserData;
            //osProfileProperty.PropertyDefinition.LinuxConfiguration.Ssh.PublicKeys.Add(new Microsoft.Azure.Management.Compute.Fluent.Models.SshPublicKey( awsInstanceProperties.KeyName, awsInstanceProperties.KeyName));
            R.AddPropertyToResource(osProfileProperty);

            //base.DoProperties();
            return R;
        }
    }
    class CAWSCloudFormationWaitConditionTranslator : CResourceTranslator
    {
        public CAWSCloudFormationWaitConditionTranslator(awsLib.awsResource awsResource) : base(awsResource)
        {

        }
        public override IArmResource Translate()
        {
            throw new NotImplementedException();
        }
    }
    class CAWSCloudFormationWaitConditionHandleTranslator : CResourceTranslator
    {
        public CAWSCloudFormationWaitConditionHandleTranslator(awsLib.awsResource awsResource) : base(awsResource)
        {

        }

        public override IArmResource Translate()
        {
            throw new NotImplementedException();
        }
    }

    // Security Group Resource Translator
    // Will take an AWS security group object and translate it into the ARM template
    class CAWSSecurityGroupTranslator : CResourceTranslator
    {
        public CAWSSecurityGroupTranslator(awsLib.awsResource awsResource) : base(awsResource)
        {

        }

        public override IArmResource Translate()
        {
            // Retrieve objects
            awsLib.SecurityGroup awsSecurityGroup = (awsLib.SecurityGroup)_awsResource;
            awsLib.SecurityGroupProperties awsSecurityGroupProperties = (awsLib.SecurityGroupProperties)awsSecurityGroup.Property;

            //Create target objects
            IArmResource R = ArmResourceFactory.CreateArmResourceByType(ArmResourceType.NetworkSecurityGroup);
            NetworkSecurityGroupInner NSGI = (NetworkSecurityGroupInner)R.ResourceDefinition;

            //Translate properties
            SecurityRuleInner SRI = new SecurityRuleInner();
            SRI.SourceAddressPrefix = TranslateUtil.ValueOrRef( awsSecurityGroupProperties.SecurityGroupIngress.CidrIp);
            SRI.DestinationAddressPrefix = TranslateUtil.ValueOrRef(awsSecurityGroupProperties.SecurityGroupIngress.CidrIp);
            SRI.SourcePortRange = TranslateUtil.ValueOrRef(awsSecurityGroupProperties.SecurityGroupIngress.FromPort.ToString());
            SRI.DestinationPortRange = TranslateUtil.ValueOrRef(awsSecurityGroupProperties.SecurityGroupIngress.ToPort.ToString());
            SRI.Protocol = TranslateUtil.ValueOrRef(awsSecurityGroupProperties.SecurityGroupIngress.IpProtocol);

            NSGI.SecurityRules = new List<SecurityRuleInner>(); ;
            NSGI.SecurityRules.Add(SRI);

            // Done
            return R;
        }
    }


    public class CResourcePropertiesTranslatorInfo
    {
        public static string DEFAULT_PROPERTY_TRANSLATOR = "CDefaultPropertyTranslator";

        public CResourcePropertiesTranslatorInfo(string aPropertyName, string aAzurePropertyName, string aTranslatorClass)
        {
            AWSPropertyName = aPropertyName;
            AzurePropertyName = aAzurePropertyName;
            TranslatorClass = aTranslatorClass;
        }

        public string AWSPropertyName;
        public string AzurePropertyName;
        public string TranslatorClass;

    }

    public class CResourceTypeTranslatorInfo
    {
        
        public string ResourceTypeName;
        public string ResourceTypeDescription;
        public string ResourceTypeTranslatorClass;

        public SortedList<String, CResourcePropertiesTranslatorInfo> Properties = new SortedList<string, CResourcePropertiesTranslatorInfo>();


        static SortedDictionary<string,CResourceTypeTranslatorInfo> _AllResourceTypeTranslators = new SortedDictionary<string, CResourceTypeTranslatorInfo>();
        public static SortedDictionary<string, CResourceTypeTranslatorInfo> AllResourceTypeTranslators { get { return _AllResourceTypeTranslators; } }
        public static void LoadAllRessourceTypes(string JSONFile)
        {
            _AllResourceTypeTranslators = JsonConvert.DeserializeObject<SortedDictionary<string, CResourceTypeTranslatorInfo>>(File.ReadAllText(JSONFile));
        }
    }

}