﻿/***********************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<http://www.gnu.org/licenses/>.
***********************************************************************/
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Text;
using Amazon;
using Amazon.CloudFormation;
using Amazon.CloudFormation.Model;
using Newtonsoft.Json.Linq;

namespace awsLib
{
    public class CloudFormation {
        private string templateUrl;
        public CloudFormation(string pathToTemplate)
        {
            this.templateUrl = pathToTemplate;
        }
        
        public awsObject ParseTemplate()
        {
            List<IAWSParameter> awsParameters = new List<IAWSParameter>();
            List<awsResource> awsResources = new List<awsResource>();
            awsObject awsObj = new awsObject();
            
            // reference name - reference type
            Dictionary<string, string> currentReferences = new Dictionary<string, string>();
            JObject jsonData; 
            var amznConfig = new AmazonCloudFormationConfig();
            amznConfig.RegionEndpoint = RegionEndpoint.USWest1;
            AmazonCloudFormationClient client = new AmazonCloudFormationClient("AKIAJG44CNEV3UUDBPTQ", "BpxbaH8Q9rXyczKCPNxE47SO/+285mTddmlD/fT0", amznConfig);
            var response = client.GetTemplateSummary(new GetTemplateSummaryRequest
            {
                TemplateURL = this.templateUrl
            });

            using (WebClient wc = new WebClient())
            {
                var data = wc.DownloadData(this.templateUrl);
                string byteData = Encoding.UTF8.GetString(data);
                jsonData = JObject.Parse(byteData);
            }

            // Get as much as possible from the AWS obj for Parameters
            // TODO: The response obj is missing some of the attributes on each secion. Must look up object in JSON manually and add. 
            foreach (var param in response.Parameters)
            {
                IAWSParameter tempParam = new awsParameter();
                tempParam.Name = param.ParameterKey;
                foreach (JProperty para in jsonData["Parameters"][param.ParameterKey])
                {
                    PropertyInfo propertyInfo = tempParam.GetType().GetProperty(para.Name);
                    if (propertyInfo != null)
                    {
                        if (propertyInfo.PropertyType.Namespace == "System.Collections.Generic")
                        {
                            var temp = para.Value.ToObject(propertyInfo.PropertyType);
                            propertyInfo.SetValue(tempParam, Convert.ChangeType(temp, propertyInfo.PropertyType), null);
                        }
                        else
                        {
                            string[] typeArray = para.Value.ToString().Split(new [] { "::" }, StringSplitOptions.None);
                            string type = typeArray[typeArray.Length - 1];
                            propertyInfo.SetValue(tempParam, Convert.ChangeType(type, propertyInfo.PropertyType), null);
                        }
                    }
                } 
               

                if (tempParam.Name != null && tempParam.Type != null)
                    currentReferences.Add(tempParam.Name, tempParam.Type);

                awsParameters.Add(tempParam);
            }

            bool firstRun = true;
            List<string> mustRevisit = new List<string>();
            while (firstRun == true || mustRevisit.Count > 0)
            {
                firstRun = false;

                foreach (JProperty child in jsonData["Resources"].Children())
                {
                    string[] tempNameArray = child.Path.Split('.');
                    string resName = tempNameArray[tempNameArray.Length - 1];
                    string[] resTypeArray = jsonData["Resources"][resName]["Type"].ToString().Split(new[] { "::" }, StringSplitOptions.None);
                    string resType = resTypeArray[resTypeArray.Length - 1];
                    if (resType == "Instance") 
                    {
                        bool unInstantiatedRef = false;
                        awsInstanceProperties instanceProperties = new awsInstanceProperties();
                        Dictionary<string, string> properties = new Dictionary<string, string>();
                        foreach (var children in jsonData["Resources"][resName]["Properties"])
                        {
                            if (unInstantiatedRef == true)
                                continue;
                            var path = children.Path.ToString().Split('.');
                            var propName = path[path.Length - 1];

                            string[] propData = cleanProperty(jsonData["Resources"]
                                                              [resName]
                                                              ["Properties"]
                                                              [propName]
                                                              .ToString());

                            foreach (var pd in propData)
                            {
                                if (propData.Length == 0)
                                    continue;
                                string[] propKeyValue = pd.Split(':');
                                string tempValue;
                                // Check for reference
                                if (propKeyValue.Length == 1)
                                    tempValue = propKeyValue[0];
                                else
                                    tempValue = propKeyValue[1];
                                if (tempValue.Contains("&"))
                                {
                                    if (!currentReferences.ContainsKey(tempValue.Substring(1)))
                                    {
                                        if (!mustRevisit.Contains(resName))
                                            mustRevisit.Add(resName);
                                        unInstantiatedRef = true;
                                        continue;
                                    }
                                    else
                                    {
                                        if (propKeyValue.Length == 1)
                                            properties.Add(propName, tempValue);
                                        else
                                            properties.Add(propKeyValue[0], propKeyValue[1]);
                                    }
                                }

                            }
                        }
                        if (unInstantiatedRef == false)
                        {
                            awsInstanceProperties awsInstanceProperties = new awsInstanceProperties();
                            foreach (var prop in properties)
                            {
                                PropertyInfo propertyInfo = awsInstanceProperties.GetType().GetProperty(prop.Key);
                                if (propertyInfo != null)
                                {
                                    propertyInfo.SetValue(awsInstanceProperties, Convert.ChangeType(prop.Value, propertyInfo.PropertyType), null);
                                }
                            }

                            Instance instance = new Instance(resName);
                            instance.Property = awsInstanceProperties;
                            awsResources.Add(instance);
                            if (mustRevisit.Contains(resName))
                                mustRevisit.Remove(resName);
                            currentReferences.Add(resName, resType);
                        }
                    }
                    else if (resType == "EIP")
                    {
                        if (!currentReferences.ContainsKey(resName))
                        {
                            EIP eip = new EIP(resName);
                            awsResources.Add(eip);
                            if (mustRevisit.Contains(resName))
                                mustRevisit.Remove(resName);
                            currentReferences.Add(resName, resType);
                        }
                    }
                    else if (resType == "SecurityGroup")
                    {
                        bool unInstantiatedRef = false;
                        Dictionary<string, string> properties = new Dictionary<string, string>();
                        if (!currentReferences.ContainsKey(resName))
                        {
                            string[] securityGroupIngressData = cleanProperty(jsonData["Resources"]
                                                                                    [resName]
                                                                                    ["Properties"]
                                                                                    ["SecurityGroupIngress"]
                                                                                    .ToString());

                            foreach (var prop in securityGroupIngressData)
                            {
                                string[] propKeyValue = prop.Split(':');
                                // Check for reference
                                if (propKeyValue[1].Contains("&"))
                                {
                                    if (!currentReferences.ContainsKey(propKeyValue[1].Substring(1)))
                                    {
                                        if (!mustRevisit.Contains(resName))
                                            mustRevisit.Add(resName);
                                        unInstantiatedRef = true;
                                        continue;
                                    }
                                }
                                // add it to dictionary
                                properties.Add(propKeyValue[0], propKeyValue[1]);
                            }

                            if (unInstantiatedRef == false)
                            {
                                var securityGroupIngress = new SecurityGroupIngress();
                                foreach (var prop in properties)
                                {
                                    PropertyInfo propertyInfo = securityGroupIngress.GetType().GetProperty(prop.Key);
                                    if (propertyInfo != null)
                                    {
                                        propertyInfo.SetValue(securityGroupIngress, Convert.ChangeType(prop.Value, propertyInfo.PropertyType), null);
                                    }
                                }

                                SecurityGroupProperties securityGroupProperties = new SecurityGroupProperties();
                                securityGroupProperties.GroupDescription = jsonData["Resources"]
                                                                                    [resName]
                                                                                    ["Properties"]
                                                                                    ["GroupDescription"].ToString();
                                securityGroupProperties.SecurityGroupIngress = securityGroupIngress;
                                var securityGroup = new SecurityGroup(resName);
                                securityGroup.Property = securityGroupProperties;
                                awsResources.Add(securityGroup);
                                if (!mustRevisit.Contains(resName))
                                    mustRevisit.Remove(resName);
                                currentReferences.Add(resName, resType);
                            }
                        }
                    }
                    else if (resType == "EIPAssociation")
                    {
                        if (!currentReferences.ContainsKey(resName))
                        {
                            bool refFound = false;
                            string[] propertyInstanceID = cleanProperty(jsonData["Resources"]
                                                        [resName]
                                                        ["Properties"]
                                                        ["InstanceId"].ToString());
                            foreach (var res in awsResources)
                            {
                                if (res.Name == propertyInstanceID[0].Substring(1))
                                    refFound = true;
                            }
                            if (refFound == false && !mustRevisit.Contains(resName))
                            {
                                mustRevisit.Add(resName);
                                continue;
                            }

                            if (refFound == true)
                            {
                                EIPAssociation eipAssociation = new EIPAssociation(resName);
                                EIPAssociationProperties eipAssociationProperties = new EIPAssociationProperties();

                                if (mustRevisit.Contains(resName))
                                    mustRevisit.Remove(resName);
                                currentReferences.Add(resName, resType);
                                awsResources.Add(eipAssociation);
                            }
                        }
                    }
                }
            }

            awsObj.Version = response.Version;
            awsObj.Description = response.Description;
            awsObj.Parameters = awsParameters;
            awsObj.Resources = awsResources;

            return awsObj;
        }

        static string[] cleanProperty(string str)
        {
            string tempStr = str.Replace("[", String.Empty)
                        .Replace("]", String.Empty)
                        .Replace("{", String.Empty)
                        .Replace("}", String.Empty)
                        .Replace("\"", String.Empty)
                        .Replace(" ", String.Empty)
                        .Replace("\n", String.Empty)
                        .Replace("\r", String.Empty)
                        //.Replace(",", String.Empty)
                        .Replace("=", ":");

            tempStr = tempStr.Replace("Ref:AWS::Region", String.Empty)
                             .Replace("Ref:", "&")
                             .Replace("Fn::Base64:", String.Empty)
                             .Replace("Fn::Join:,", String.Empty)
                             .Replace("Fn::FindInMap:", String.Empty)
                             .Replace("AWSRegionArch2AMI", String.Empty)
                             .Replace("AWSInstanceType2Arch", String.Empty)
                             .Replace("&InstanceTypeArch", String.Empty);

            return tempStr.Split(',');
        }
 
        static void Main(string[] args)
        {

            var test = new CloudFormation(@"https://s3-us-west-2.amazonaws.com/cloudformation-templates-us-west-2/EIP_With_Association.template");


            AwsLib.Core.AWS2ARM.CResourceTypeTranslatorInfo.LoadAllRessourceTypes("AWS2ARM\\ResourceTranslatorInfos.json");
            AwsLib.Core.AWS2ARM.CParameterTranslatorInfo.LoadAllParametersInfo("AWS2ARM\\ParamTranslatorInfos.json");


            AwsLib.Core.AWS2ARM.CTemplateTranslator C = new AwsLib.Core.AWS2ARM.CTemplateTranslator(test.ParseTemplate());
            var armTemplate = C.Translate();




        }
    }
}