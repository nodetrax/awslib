﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AwsLib.Core.AzureResourceManager
{
    /// <summary>
    /// Represents an ARM parameter.
    /// </summary>
    public class ArmParameter : IArmParameter
    {
        public ArmParameter()
        {
            this.AllowedValues = new List<JToken>();
        }

        public ArmParameter(List<JToken> allowedValues)
        {
            this.AllowedValues = allowedValues;
        }

        [JsonIgnore]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "defaultValue")]
        public JToken DefaultValue { get; set; }

        [JsonProperty(PropertyName = "allowedValues")]
        public List<JToken> AllowedValues { get; }

        [JsonProperty(PropertyName = "metadata.description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "minLength")]
        public int MinLength { get; set; }

        [JsonProperty(PropertyName = "maxLength")]
        public int MaxLength { get; set; }

        public bool ShouldSerializeAllowedValues()
        {
            return this.AllowedValues.Count > 0;
        }
    }
}
