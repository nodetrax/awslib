﻿using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Rest.Azure;
using Newtonsoft.Json;

namespace AwsLib.Core.AzureResourceManager
{
    /// <summary>
    /// Model representing an ARM Template.
    /// </summary>
    public class ArmTemplateDefinition : IArmTemplateDefinition
    {
        private const string DefaultSchemaUri = "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#";
        private const string DefaultContentVersion = "1.0.0.0";

        [JsonProperty(PropertyName = "$schema")]
        public string Schema { get; set; } = DefaultSchemaUri;

        [JsonProperty(PropertyName = "contentVersion")]
        public string ContentVersion { get; set; } = DefaultContentVersion;

        [JsonIgnore]
        public List<IArmResource> Resources { get; } = new List<IArmResource>();

        [JsonProperty(PropertyName = "resources")]
        public List<IResource> ResourcesInner { get; } = new List<IResource>();

        [JsonIgnore]
        public IList<IArmParameter> Parameters { get; } = new List<IArmParameter>();

        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, IArmParameter> ParametersInner { get; } = new Dictionary<string, IArmParameter>();

        [JsonIgnore]
        public IList<IArmOutput> Outputs { get; } = new List<IArmOutput>();

        [JsonProperty(PropertyName = "outputs")]
        public Dictionary<string, IArmOutput> OutputsInner { get; } = new Dictionary<string, IArmOutput>();

        [JsonProperty(PropertyName = "variables")]
        public List<IArmVariable> Variables { get; } = new List<IArmVariable>();

        public bool ShouldSerializeParametersInner()
        {
            return this.Parameters.Count > 0;
        }

        public bool ShouldSerializeOutputsInner()
        {
            return this.Outputs.Count > 0;
        }

        public bool ShouldSerializeVariables()
        {
            return this.Variables.Count > 0;
        }
    }
}
