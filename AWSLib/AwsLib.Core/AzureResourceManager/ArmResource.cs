﻿using System;
using System.Collections.Generic;
using AwsLib.Core.AzureResourceManager.ArmProperties;
using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.Network.Fluent.Models;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Newtonsoft.Json;

namespace AwsLib.Core.AzureResourceManager
{
    /// <summary>
    /// Represent an ARM resource.
    /// </summary>
    public class ArmResource : IArmResource
    {
        [JsonIgnore]
        public Resource ResourceDefinition { get; set; }

        public void AddPropertyToResource<T>(IArmProperty<T> armProperty)
        {
            var virtualMachineDefinition = this.ResourceDefinition as VirtualMachineInner;
            if (virtualMachineDefinition != null)
            {
                AddVirtualMachineProperty(virtualMachineDefinition, armProperty);
                return;
            }

            var virtualNetworkDefinition = this.ResourceDefinition as VirtualNetworkInner;
            if (virtualNetworkDefinition != null)
            {
                AddVirtualNetworkProperty(virtualNetworkDefinition, armProperty);
                return;
            }

            var nsgDefinition = this.ResourceDefinition as NetworkSecurityGroupInner;
            if (nsgDefinition != null)
            {
                AddNetworkSecurityGroupProperty(nsgDefinition, armProperty);
                return;
            }

            var pipDefinition = this.ResourceDefinition as PublicIPAddressInner;
            if (pipDefinition != null)
            {
                AddPublicIpAddressProperty(pipDefinition, armProperty);
                return;
            }

            var nicDefinition = this.ResourceDefinition as NetworkInterfaceInner;
            if (nicDefinition != null)
            {
                AddNetworkInterfaceProperty(nicDefinition, armProperty);
                return;
            }

            throw new NotSupportedException("The resource type provided is not supported.");
        }

        private void AddVirtualMachineProperty<T>(VirtualMachineInner virtualMachineDefinition, IArmProperty<T> armProperty)
        {
            Type propertyType = armProperty.GetType();

            if (propertyType == typeof(HardwareProfileProperty))
            {
                virtualMachineDefinition.HardwareProfile = ((HardwareProfileProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(OSProfileProperty))
            {
                virtualMachineDefinition.OsProfile = ((OSProfileProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(StorageProfileProperty))
            {
                virtualMachineDefinition.StorageProfile = ((StorageProfileProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(NetworkProfileProperty))
            {
                virtualMachineDefinition.NetworkProfile = ((NetworkProfileProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(DiagnosticsProfileProperty))
            {
                virtualMachineDefinition.DiagnosticsProfile = ((DiagnosticsProfileProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(AvailabilitySetProperty))
            {
                virtualMachineDefinition.AvailabilitySet = ((AvailabilitySetProperty)armProperty).PropertyDefinition;
                return;
            }

            throw new NotSupportedException("The virtual machine property provided is not supported.");
        }

        private void AddVirtualNetworkProperty<T>(VirtualNetworkInner virtualNetworkDefinition, IArmProperty<T> armProperty)
        {
            Type propertyType = armProperty.GetType();

            if (propertyType == typeof(AddressSpaceProperty))
            {
                virtualNetworkDefinition.AddressSpace = ((AddressSpaceProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(DhcpOptionsProperty))
            {
                virtualNetworkDefinition.DhcpOptions = ((DhcpOptionsProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(SubnetsProperty))
            {
                virtualNetworkDefinition.Subnets = ((SubnetsProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(VirtualNetworkPeeringsProperty))
            {
                virtualNetworkDefinition.VirtualNetworkPeerings = ((VirtualNetworkPeeringsProperty)armProperty).PropertyDefinition;
                return;
            }

            throw new NotSupportedException("The virtual network property provided is not supported.");
        }

        private void AddNetworkSecurityGroupProperty<T>(NetworkSecurityGroupInner nsgDefinition, IArmProperty<T> armProperty)
        {
            Type propertyType = armProperty.GetType();

            if (propertyType == typeof(SecurityRulesProperty))
            {
                nsgDefinition.SecurityRules = ((SecurityRulesProperty)armProperty).PropertyDefinition;
                return;
            }

            throw new NotSupportedException("The network security group property provided is not supported.");
        }

        private void AddPublicIpAddressProperty<T>(PublicIPAddressInner pipDefinition, IArmProperty<T> armProperty)
        {
            Type propertyType = armProperty.GetType();

            if (propertyType == typeof(IpAddressProperty))
            {
                pipDefinition.IpAddress = ((IpAddressProperty)armProperty).PropertyDefinition;
                return;
            }

            throw new NotSupportedException("The network security group property provided is not supported.");
        }

        private void AddNetworkInterfaceProperty<T>(NetworkInterfaceInner nicDefinition, IArmProperty<T> armProperty)
        {
            Type propertyType = armProperty.GetType();

            if (propertyType == typeof(IpConfigurationsProperty))
            {
                nicDefinition.IpConfigurations = ((IpConfigurationsProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(DnsSettingsProperty))
            {
                nicDefinition.DnsSettings = ((DnsSettingsProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(NetworkSecurityGroupProperty))
            {
                nicDefinition.NetworkSecurityGroup = ((NetworkSecurityGroupProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(DnsSettingsProperty))
            {
                nicDefinition.DnsSettings = ((DnsSettingsProperty)armProperty).PropertyDefinition;
                return;
            }

            if (propertyType == typeof(MacAddressProperty))
            {
                nicDefinition.MacAddress = ((MacAddressProperty)armProperty).PropertyDefinition;
                return;
            }

            throw new NotSupportedException("The network interface property provided is not supported.");
        }
    }
}
