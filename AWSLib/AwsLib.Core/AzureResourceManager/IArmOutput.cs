﻿using Newtonsoft.Json.Linq;

namespace AwsLib.Core.AzureResourceManager
{
    public interface IArmOutput
    {
        string Name { get; set; }

        string OutputType { get; set; }

        JValue OutputValue { get; set; }
    }
}
