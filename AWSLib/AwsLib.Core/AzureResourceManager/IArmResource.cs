﻿using Microsoft.Azure.Management.ResourceManager.Fluent;

namespace AwsLib.Core.AzureResourceManager
{
    public interface IArmResource
    {
        Resource ResourceDefinition { get; set; }

        void AddPropertyToResource<T>(IArmProperty<T> armProperty);
    }
}
