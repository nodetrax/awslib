﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Microsoft.Rest.Azure;
using Microsoft.Rest.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AwsLib.Core.AzureResourceManager
{
    /// <summary>
    /// ARM template structure and functionality.
    /// </summary>
    public class ArmTemplate
    {
        public ArmTemplate()
        {
            this.ArmTemplateDefinition = new ArmTemplateDefinition();
        }

        /// <summary>
        /// Gets or sets the definition for an ARM template.
        /// </summary>
        public ArmTemplateDefinition ArmTemplateDefinition { get; set; }

        public ArmTemplate(ArmTemplateDefinition definition)
        {
            this.ArmTemplateDefinition = definition;
        }

        public IArmResource CreateArmResource(ArmResourceType resourceType, string name = null, string location = null)
        {
            return ArmResourceFactory.CreateArmResourceByType(resourceType, name, location);
        }

        /// <summary>
        /// Adds an ARM resource definition to the current ARM template.
        /// </summary>
        /// <param name="resource">The ARM resource to add to the template.</param>
        public void AddResourceToTemplate(IArmResource resource)
        {
            this.ArmTemplateDefinition.Resources.Add(resource);
        }

        /// <summary>
        /// Generates the JSON payload corresponding to the current ARM template.
        /// </summary>
        /// <returns></returns>
        public string GetJson(bool includeSampleOutputs = false)
        {
            if (includeSampleOutputs)
            {
                this.AddSampleOutputs();
            }

            this.PrepareInternalFields();

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new TransformationJsonConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.Indented;

                serializer.Serialize(writer, this.ArmTemplateDefinition);
            }

            string serialized = sb.ToString();
            return serialized;
        }

        private void PrepareInternalFields()
        {
            this.ArmTemplateDefinition.ParametersInner.Clear();
            foreach (var parameter in this.ArmTemplateDefinition.Parameters)
            {
                this.ArmTemplateDefinition.ParametersInner.Add(parameter.Name, parameter);
            }

            this.ArmTemplateDefinition.OutputsInner.Clear();
            foreach (var output in this.ArmTemplateDefinition.Outputs)
            {
                this.ArmTemplateDefinition.OutputsInner.Add(output.Name, output);
            }

            foreach (var armResource in this.ArmTemplateDefinition.Resources)
            {
                this.ArmTemplateDefinition.ResourcesInner.Add(armResource.ResourceDefinition);
            }
        }

        private void AddSampleOutputs()
        {
            this.ArmTemplateDefinition.Outputs.Add(new ArmOutput() { Name = "InstanceId", OutputType = "string", OutputValue = new JValue("Canonical:UbuntuServer:14.04.2-LTS") });
            this.ArmTemplateDefinition.Outputs.Add(new ArmOutput() { Name = "InstanceIPAddress", OutputType = "string", OutputValue = new JValue("192.168.1.1") });
        }
    }
}
