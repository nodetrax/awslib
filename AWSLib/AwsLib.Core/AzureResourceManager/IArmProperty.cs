﻿namespace AwsLib.Core.AzureResourceManager
{
    public interface IArmProperty<T>
    {
        T PropertyDefinition { get; set; }
    }
}
