﻿using System.Collections.Generic;

namespace AwsLib.Core.AzureResourceManager
{
    public interface IArmTemplateDefinition
    {
        string Schema { get; set; }

        string ContentVersion { get; set; }

        List<IArmResource> Resources { get; }

        IList<IArmParameter> Parameters { get; }

        Dictionary<string, IArmParameter> ParametersInner { get; }

        IList<IArmOutput> Outputs { get; }

        Dictionary<string, IArmOutput> OutputsInner { get; }
        
        List<IArmVariable> Variables { get; }
    }
}
