﻿using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.ResourceManager.Fluent;

namespace AwsLib.Core.AzureResourceManager.ArmProperties
{
    public class HardwareProfileProperty : IArmProperty<HardwareProfile>
    {
        public HardwareProfile PropertyDefinition { get; set; } = new HardwareProfile();
    }

    public class StorageProfileProperty : IArmProperty<StorageProfile>
    {
        public StorageProfile PropertyDefinition { get; set; } = new StorageProfile();
    }

    public class OSProfileProperty : IArmProperty<OSProfile>
    {
        public OSProfile PropertyDefinition { get; set; } = new OSProfile();
    }

    public class NetworkProfileProperty : IArmProperty<NetworkProfile>
    {
        public NetworkProfile PropertyDefinition { get; set; } = new NetworkProfile();
    }

    public class DiagnosticsProfileProperty : IArmProperty<DiagnosticsProfile>
    {
        public DiagnosticsProfile PropertyDefinition { get; set; } = new DiagnosticsProfile();
    }

    public class AvailabilitySetProperty : IArmProperty<SubResource>
    {
        public SubResource PropertyDefinition { get; set; } = new SubResource();
    }
}
