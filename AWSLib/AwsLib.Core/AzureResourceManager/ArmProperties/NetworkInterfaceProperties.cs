﻿using System.Collections.Generic;
using Microsoft.Azure.Management.Network.Fluent.Models;
using Microsoft.Azure.Management.ResourceManager.Fluent;

namespace AwsLib.Core.AzureResourceManager.ArmProperties
{
    public class VirtualMachineProperty : IArmProperty<SubResource>
    {
        public SubResource PropertyDefinition { get; set; } = new SubResource();
    }

    public class NetworkSecurityGroupProperty : IArmProperty<SubResource>
    {
        public SubResource PropertyDefinition { get; set; } = new SubResource();
    }

    public class IpConfigurationsProperty : IArmProperty<IList<NetworkInterfaceIPConfigurationInner>>
    {
        public IList<NetworkInterfaceIPConfigurationInner> PropertyDefinition { get; set; } = new List<NetworkInterfaceIPConfigurationInner>();
    }

    public class DnsSettingsProperty : IArmProperty<NetworkInterfaceDnsSettings>
    {
        public NetworkInterfaceDnsSettings PropertyDefinition { get; set; } = new NetworkInterfaceDnsSettings();
    }

    public class MacAddressProperty : IArmProperty<string>
    {
        public string PropertyDefinition { get; set; } = string.Empty;
    }
}
