﻿using System.Collections.Generic;
using Microsoft.Azure.Management.Network.Fluent.Models;

namespace AwsLib.Core.AzureResourceManager.ArmProperties
{
    public class AddressSpaceProperty : IArmProperty<AddressSpace>
    {
        public AddressSpace PropertyDefinition { get; set; } = new AddressSpace();
    }

    public class DhcpOptionsProperty : IArmProperty<DhcpOptions>
    {
        public DhcpOptions PropertyDefinition { get; set; } = new DhcpOptions();
    }

    public class SubnetsProperty : IArmProperty<IList<SubnetInner>>
    {
        public IList<SubnetInner> PropertyDefinition { get; set; } = new List<SubnetInner>();
    }

    public class VirtualNetworkPeeringsProperty : IArmProperty<IList<VirtualNetworkPeeringInner>>
    {
        public IList<VirtualNetworkPeeringInner> PropertyDefinition { get; set; } = new List<VirtualNetworkPeeringInner>();
    }
}
