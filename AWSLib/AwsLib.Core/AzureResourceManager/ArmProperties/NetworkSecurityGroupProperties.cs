﻿using System.Collections.Generic;
using Microsoft.Azure.Management.Network.Fluent.Models;

namespace AwsLib.Core.AzureResourceManager.ArmProperties
{
    public class SecurityRulesProperty : IArmProperty<IList<SecurityRuleInner>>
    {
        public IList<SecurityRuleInner> PropertyDefinition { get; set; } = new List<SecurityRuleInner>();
    }

    public class DefaultSecurityRulesProperty : IArmProperty<IList<SecurityRuleInner>>
    {
        public IList<SecurityRuleInner> PropertyDefinition { get; set; } = new List<SecurityRuleInner>();
    }

    public class NetworkInterfacesProperty : IArmProperty<IList<NetworkInterfaceInner>>
    {
        public IList<NetworkInterfaceInner> PropertyDefinition { get; set; } = new List<NetworkInterfaceInner>();
    }

    public class NsgSubnetsProperty : IArmProperty<IList<SubnetInner>>
    {
        public IList<SubnetInner> PropertyDefinition { get; set; } = new List<SubnetInner>();
    }
}
