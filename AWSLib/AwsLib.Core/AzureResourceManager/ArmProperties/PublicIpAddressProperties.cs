﻿namespace AwsLib.Core.AzureResourceManager.ArmProperties
{
    public class IpAddressProperty : IArmProperty<string>
    {
        public string PropertyDefinition { get; set; } = string.Empty;
    }
}
