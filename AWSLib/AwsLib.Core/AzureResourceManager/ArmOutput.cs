﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AwsLib.Core.AzureResourceManager
{
    public class ArmOutput : IArmOutput
    {
        [JsonIgnore]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string OutputType { get; set; }

        [JsonProperty(PropertyName = "value")]
        public JValue OutputValue { get; set; }
    }
}
