﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace AwsLib.Core.AzureResourceManager
{
    public interface IArmParameter
    {
        string Name { get; set; }

        string Type { get; set; }

        JToken DefaultValue { get; set; }

        List<JToken> AllowedValues { get; }

        string Description { get; set; }

        int MinLength { get; set; }

        int MaxLength { get; set; }
    }
}
