﻿using System;
using System.Runtime.Remoting.Messaging;
using AwsLib.Core.AzureResourceManager.ArmProperties;
using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.Network.Fluent.Models;
using Microsoft.Azure.Management.Storage.Fluent.Models;

namespace AwsLib.Core.AzureResourceManager
{
    public static class ArmResourceFactory
    {
        private const string DefaultAzureRegion = "westus";
        private const string DefaultAzureVmSize = "Standard_F1";

        private const string AvailabilitySetType = "Microsoft.Compute/availabilitySets";
        private const string StorageAccountType = "Microsoft.Storage/storageAccounts";
        private const string VirtualNetworkType = "Microsoft.Network/virtualNetworks";
        private const string NetworkInterfaceType = "Microsoft.Network/networkInterfaces";
        private const string VirtualMachineType = "Microsoft.Compute/virtualMachines";
        private const string NetworkSecurityGroupType = "Microsoft.Network/networkSecurityGroups";
        private const string PublicIpType = "Microsoft.Network/publicIPAddresses";
        private const string LoadBalancerType = "Microsoft.Network/loadBalancers";

        public static IArmResource CreateArmResourceByType(ArmResourceType resourceType, string name = null, string location = null)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                name = GenerateRandomName(resourceType);
            }

            if (string.IsNullOrWhiteSpace(location))
            {
                location = DefaultAzureRegion;
            }

            // A simplified approach to constructing the different resource types supported.
            switch (resourceType)
            {
                case ArmResourceType.VirtualMachine:
                    IArmResource vmResource = new ArmResource() { ResourceDefinition = new VirtualMachineInner(name: name, location: location, type: VirtualMachineType) };
                    var storageProfileProperty = new StorageProfileProperty
                    {
                        PropertyDefinition =
                        {
                            ImageReference = new ImageReferenceInner(
                                sku: "14.04.2-LTS", offer: "UbuntuServer", publisher: "Canonical", version: "latest"
                            )
                        }
                    };
                    vmResource.AddPropertyToResource(storageProfileProperty);

                    var hardwareProfileProperty = new HardwareProfileProperty();
                    hardwareProfileProperty.PropertyDefinition.VmSize = DefaultAzureVmSize;
                    vmResource.AddPropertyToResource(hardwareProfileProperty);

                    return vmResource;
                case ArmResourceType.AvailabilitySet:
                    IArmResource asResource = new ArmResource() { ResourceDefinition = new AvailabilitySetInner(name: name, location: location, type: AvailabilitySetType) };
                    return asResource;
                case ArmResourceType.StorageAccount:
                    IArmResource storageResource = new ArmResource() { ResourceDefinition = new StorageAccountInner(name: name, location: location, type: StorageAccountType) };
                    return storageResource;
                case ArmResourceType.VirtualNetwork:
                    IArmResource vnetResource = new ArmResource() { ResourceDefinition = new VirtualNetworkInner(name: name, location: location, type: VirtualNetworkType) };
                    return vnetResource;
                case ArmResourceType.NetworkInterface:
                    IArmResource nicResource = new ArmResource() { ResourceDefinition = new NetworkInterfaceInner(name: name, location: location, type: NetworkInterfaceType) };
                    return nicResource;
                case ArmResourceType.NetworkSecurityGroup:
                    IArmResource nsgResource = new ArmResource() { ResourceDefinition = new NetworkSecurityGroupInner(name: name, location: location, type: NetworkSecurityGroupType) };
                    return nsgResource;
                case ArmResourceType.PublicIp:
                    IArmResource pipResource = new ArmResource() { ResourceDefinition = new PublicIPAddressInner(name: name, location: location, type: PublicIpType) };
                    return pipResource;
                case ArmResourceType.LoadBalancer:
                    IArmResource lbResource = new ArmResource() { ResourceDefinition = new LoadBalancerInner(name: name, location: location, type: LoadBalancerType) };
                    return lbResource;
                default:
                    throw new NotSupportedException("Specified resource type is not supported.");
            }
        }

        private static string GenerateRandomName(ArmResourceType resourceType)
        {
            int randomNumber = new Random((int) DateTime.Now.Ticks).Next(1000000, 9999999);

            switch (resourceType)
            {
                case ArmResourceType.VirtualMachine:
                    return $"vm{randomNumber}";
                case ArmResourceType.AvailabilitySet:
                    return $"avset{randomNumber}";
                case ArmResourceType.StorageAccount:
                    return $"stor{randomNumber}";
                case ArmResourceType.VirtualNetwork:
                    return $"vnet{randomNumber}";
                case ArmResourceType.NetworkInterface:
                    return $"nic{randomNumber}";
                case ArmResourceType.NetworkSecurityGroup:
                    return $"nsg{randomNumber}";
                case ArmResourceType.PublicIp:
                    return $"pip{randomNumber}";
                case ArmResourceType.LoadBalancer:
                    return $"lb{randomNumber}";
                default:
                    return $"az{randomNumber}";
            }
        }
    }
}
