﻿namespace AwsLib.Core.AzureResourceManager
{
    public enum ArmResourceType
    {
        None,
        VirtualMachine,
        AvailabilitySet,
        StorageAccount,
        VirtualNetwork,
        NetworkInterface,
        NetworkSecurityGroup,
        PublicIp,
        LoadBalancer
    }
}
