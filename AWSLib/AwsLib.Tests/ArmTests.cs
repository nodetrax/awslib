﻿using System;
using System.Collections.Generic;
using AwsLib.Core.AzureResourceManager;
using AwsLib.Core.AzureResourceManager.ArmProperties;
using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.Network.Fluent.Models;
using Microsoft.Azure.Management.Storage.Fluent.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AwsLib.Tests
{
    [TestClass]
    public class ArmTests
    {
        private const string EmptyArmTemplate = "{\r\n  \"$schema\": \"http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#\",\r\n  \"contentVersion\": \"1.0.0.0\",\r\n  \"resources\": []\r\n}";
        private const string DefaultSchemaUri = "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#";
        private const string DefaultContentVersion = "1.0.0.0";

        [TestMethod]
        [TestCategory("ARM")]
        public void GenerateBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.AreEqual(json, EmptyArmTemplate, $"JSON payload should resemble an empty ARM template ({EmptyArmTemplate}).");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void GenerateSimpleArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            template.ArmTemplateDefinition.Schema = DefaultSchemaUri;
            template.ArmTemplateDefinition.ContentVersion = DefaultContentVersion;
            template.ArmTemplateDefinition.Outputs.Add(new ArmOutput() { Name = "output1", OutputType = "string", OutputValue = new JValue("value1") });
            template.ArmTemplateDefinition.Parameters.Add(new ArmParameter() { Name = "parameter1" });
            template.ArmTemplateDefinition.Variables.Add(new ArmVariable());
            template.ArmTemplateDefinition.Resources.Add(new ArmResource());
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void GenerateSimpleArmTemplateDefinitionTest()
        {
            List<JToken> allowedValues = new List<JToken>();
            allowedValues.Add("value1");
            allowedValues.Add("value2");
            allowedValues.Add("value3");

            var parameter = new ArmParameter(allowedValues);
            parameter.Name = "parameter2";
            parameter.Type = "String";
            parameter.DefaultValue = "value2";
            parameter.Description = "Sample description";

            ArmTemplateDefinition definition = new ArmTemplateDefinition();
            definition.Schema = DefaultSchemaUri;
            definition.ContentVersion = DefaultContentVersion;
            definition.Outputs.Add(new ArmOutput() { Name = "output1", OutputType = "string", OutputValue = new JValue("value1") });
            definition.Outputs.Add(new ArmOutput() { Name = "output2", OutputType = "bool", OutputValue = new JValue("value2") });
            definition.Parameters.Add(new ArmParameter() { Name = "parameter1" });
            definition.Parameters.Add(parameter);
            definition.Variables.Add(new ArmVariable());
            definition.Resources.Add(new ArmResource());
            ArmTemplate template = new ArmTemplate(definition);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddVirtualMachineToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.VirtualMachine, "vm1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as VirtualMachineInner, "ResourceDefinition must be an instance of VirtualMachineInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddAvailabilitySetToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.AvailabilitySet, "availset1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as AvailabilitySetInner, "ResourceDefinition must be an instance of AvailabilitySetInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddStorageAccountToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.StorageAccount, "storage1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as StorageAccountInner, "ResourceDefinition must be an instance of StorageAccountInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddVirtualNetworkToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.VirtualNetwork, "vnet1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as VirtualNetworkInner, "ResourceDefinition must be an instance of VirtualNetworkInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddNetworkInterfaceToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.NetworkInterface, "nic1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as NetworkInterfaceInner, "ResourceDefinition must be an instance of NetworkInterfaceInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddNetworkSecurityGroupToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.NetworkSecurityGroup, "nsg1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as NetworkSecurityGroupInner, "ResourceDefinition must be an instance of NetworkSecurityGroupInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPublicIpToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.PublicIp, "pip1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as PublicIPAddressInner, "ResourceDefinition must be an instance of PublicIPAddressInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddLoadBalancerToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.LoadBalancer, "lb1");
            template.AddResourceToTemplate(resource);
            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
            Assert.IsNotNull(resource.ResourceDefinition as LoadBalancerInner, "ResourceDefinition must be an instance of LoadBalancerInner.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        [ExpectedException(typeof(NotSupportedException), "None is not supported and should return an NotSupportedException.", AllowDerivedTypes = true)]
        public void AddUnsupportedResourceTypeToBlankArmTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.None);
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void GenerateArmParameterTest()
        {
            var parameter = new ArmParameter();
            parameter.Name = "parameter1";
            parameter.Type = "String";
            parameter.DefaultValue = "value1";
            parameter.AllowedValues.Add("value1");
            parameter.AllowedValues.Add("value2");
            parameter.Description = "Sample description";
            parameter.MinLength = 1;
            parameter.MaxLength = 100;

            string json = JsonConvert.SerializeObject(parameter);

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void GenerateArmParameterWithAllowedValuesTest()
        {
            List<JToken> allowedValues = new List<JToken>();
            allowedValues.Add("value1");
            allowedValues.Add("value2");
            allowedValues.Add("value3");

            var parameter = new ArmParameter(allowedValues);
            parameter.Name = "parameter2";
            parameter.Type = "String";
            parameter.DefaultValue = "value2";
            parameter.Description = "Sample description";

            string json = JsonConvert.SerializeObject(parameter);

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPropertiesToVirtualMachineTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.VirtualMachine, "vm1");
            template.AddResourceToTemplate(resource);

            var hardwareProfileProperty = new HardwareProfileProperty();
            hardwareProfileProperty.PropertyDefinition.VmSize = "Standard_F1";
            resource.AddPropertyToResource(hardwareProfileProperty);

            var osProfileProperty = new OSProfileProperty();
            osProfileProperty.PropertyDefinition.AdminUsername = "jdoe";
            osProfileProperty.PropertyDefinition.AdminPassword = "shsshsecret";
            osProfileProperty.PropertyDefinition.ComputerName = "vm1";
            osProfileProperty.PropertyDefinition.CustomData = "my custom data";
            var sshKeys = new List<SshPublicKey>();
            sshKeys.Add(new SshPublicKey("keyname"));
            osProfileProperty.PropertyDefinition.LinuxConfiguration = new LinuxConfiguration(
                disablePasswordAuthentication: true, ssh: new SshConfiguration(sshKeys));
            resource.AddPropertyToResource(osProfileProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPropertiesToVirtualNetworkTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.VirtualNetwork, "vnet1");
            template.AddResourceToTemplate(resource);

            var addressSpaceProperty = new AddressSpaceProperty();
            var addressPrefixes = new List<string>();
            addressPrefixes.Add("10.0.0.0/16");
            addressSpaceProperty.PropertyDefinition.AddressPrefixes = addressPrefixes;
            resource.AddPropertyToResource(addressSpaceProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddUbuntuImageReferenceToVirtualMachineTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.VirtualMachine, "vm1");
            template.AddResourceToTemplate(resource);

            var storageProfileProperty = new StorageProfileProperty
            {
                PropertyDefinition =
                {
                    ImageReference = new ImageReferenceInner(
                        sku: "14.04.2-LTS", offer: "UbuntuServer", publisher: "Canonical", version: "latest"
                    )
                }
            };
            resource.AddPropertyToResource(storageProfileProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPropertiesToNetworkSecurityGroupTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.NetworkSecurityGroup, "nsg1");
            template.AddResourceToTemplate(resource);

            var securityRulesProperty = new SecurityRulesProperty();
            var securityRules = new List<SecurityRuleInner>();
            var securityRule = new SecurityRuleInner();
            securityRule.Protocol = "tcp";
            securityRule.SourcePortRange = "22";
            securityRule.SourceAddressPrefix = "0.0.0.0/0";
            securityRule.DestinationPortRange = "22";
            securityRule.SourceAddressPrefix = "0.0.0.0/0";
            securityRules.Add(securityRule);
            securityRulesProperty.PropertyDefinition = securityRules;
            resource.AddPropertyToResource(securityRulesProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPropertiesToPublicIpAddressTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.PublicIp, "pip1");
            template.AddResourceToTemplate(resource);

            var ipAddressProperty = new IpAddressProperty();
            ipAddressProperty.PropertyDefinition = "192.168.1.1";
            resource.AddPropertyToResource(ipAddressProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddPropertiesToNetworkInterfaceTest()
        {
            ArmTemplate template = new ArmTemplate();
            var resource = template.CreateArmResource(ArmResourceType.NetworkInterface, "nic1");
            template.AddResourceToTemplate(resource);

            var macAddressProperty = new MacAddressProperty();
            macAddressProperty.PropertyDefinition = "00-00-00-00-00-00";
            resource.AddPropertyToResource(macAddressProperty);

            var ipConfigurationsProperty = new IpConfigurationsProperty();
            var ipConfigurations = new List<NetworkInterfaceIPConfigurationInner>();
            ipConfigurations.Add(new NetworkInterfaceIPConfigurationInner() { Name = "ipconfig1", PrivateIPAddress = "10.0.0.2" } );
            ipConfigurationsProperty.PropertyDefinition = ipConfigurations;
            resource.AddPropertyToResource(ipConfigurationsProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

        [TestMethod]
        [TestCategory("ARM")]
        public void AddMultipleResourcesToTemplateTest()
        {
            ArmTemplate template = new ArmTemplate();
            var vmResource = template.CreateArmResource(ArmResourceType.VirtualMachine, "vm1");
            template.AddResourceToTemplate(vmResource);

            var hardwareProfileProperty = new HardwareProfileProperty();
            hardwareProfileProperty.PropertyDefinition.VmSize = "Standard_F1";
            vmResource.AddPropertyToResource(hardwareProfileProperty);

            var osProfileProperty = new OSProfileProperty();
            osProfileProperty.PropertyDefinition.AdminUsername = "jdoe";
            osProfileProperty.PropertyDefinition.AdminPassword = "shsshsecret";
            osProfileProperty.PropertyDefinition.ComputerName = "vm1";
            osProfileProperty.PropertyDefinition.CustomData = "my custom data";
            var sshKeys = new List<SshPublicKey>();
            sshKeys.Add(new SshPublicKey("keyname"));
            osProfileProperty.PropertyDefinition.LinuxConfiguration = new LinuxConfiguration(
                disablePasswordAuthentication: true, ssh: new SshConfiguration(sshKeys));
            vmResource.AddPropertyToResource(osProfileProperty);

            var pipResource = template.CreateArmResource(ArmResourceType.PublicIp, "pip1");
            template.AddResourceToTemplate(pipResource);

            var ipAddressProperty = new IpAddressProperty();
            ipAddressProperty.PropertyDefinition = "192.168.1.1";
            pipResource.AddPropertyToResource(ipAddressProperty);

            string json = template.GetJson();

            Assert.IsFalse(string.IsNullOrWhiteSpace(json), "JSON payload should not be an empty string. Valid JSON expected.");
        }

    }
}
