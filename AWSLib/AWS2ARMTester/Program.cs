﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwsLib.Core;
using AwsLib.Core.AWS2ARM;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using awsLib;
using Amazon;
using Amazon.CloudFormation;
using Amazon.CloudFormation.Model;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Reflection;

namespace AWS2ARMTester
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateInfos();
            LoadInfos();
            Console.WriteLine("Number of Param Handlers:" + CParameterTranslatorInfo.AllParameterTranslators.Count.ToString());
            Console.WriteLine("Number of Resource Handlers:" + CResourceTypeTranslatorInfo.AllResourceTypeTranslators.Count.ToString());
            Console.ReadLine();

            var test = new CloudFormation(@"https://s3-us-west-2.amazonaws.com/cloudformation-templates-us-west-2/EIP_With_Association.template");
            test.ParseTemplate();

            awsObject O = test.awsObject;

            AwsLib.Core.AWS2ARM.CTemplateTranslator C = new CTemplateTranslator(O);
            var armTemplate = C.Translate();


        }

        private static void LoadInfos()
        {
            CResourceTypeTranslatorInfo.LoadAllRessourceTypes("AWS2ARM\\ResourceTranslatorInfos.json");
            CParameterTranslatorInfo.LoadAllParametersInfo("AWS2ARM\\ParamTranslatorInfos.json");
        }        


        private static void CreateInfos()
        { 
            var L = CResourceTypeTranslatorInfo.AllResourceTypeTranslators;

            CResourceTypeTranslatorInfo RTT;

            RTT = new CResourceTypeTranslatorInfo();
            RTT.ResourceTypeName = "AWS::EC2::Instance";
            RTT.ResourceTypeDescription = "A Virtual Machine";
            RTT.ResourceTypeTranslatorClass = "CAWSEC2InstanceTranslator";
            RTT.Properties.Add("ImageId", new CResourcePropertiesTranslatorInfo("ImageId", "ImageId", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            RTT.Properties.Add("KeyName", new CResourcePropertiesTranslatorInfo("KeyName", "KeyName", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            RTT.Properties.Add("InstanceType", new CResourcePropertiesTranslatorInfo("InstanceType", "InstanceType", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            RTT.Properties.Add("SecurityGroup", new CResourcePropertiesTranslatorInfo("SecurityGroup", "SecurityGroup", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            RTT.Properties.Add("UserData", new CResourcePropertiesTranslatorInfo("UserData", "UserData", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            L.Add(RTT.ResourceTypeName, RTT);

            RTT = new CResourceTypeTranslatorInfo();
            RTT.ResourceTypeName = "AWS::SQS::Queue";
            RTT.ResourceTypeDescription = "A Queue";
            RTT.ResourceTypeTranslatorClass = "CQueueTranslator";
            RTT.Properties.Add("QueueName", new CResourcePropertiesTranslatorInfo("QueueName", "QueueName", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            L.Add(RTT.ResourceTypeName, RTT);


            RTT = new CResourceTypeTranslatorInfo();
            RTT.ResourceTypeName = "AWS::SNS::Topic";
            RTT.ResourceTypeDescription = "A Topic";
            RTT.ResourceTypeTranslatorClass = "CTopicTranslator";
            RTT.Properties.Add("TopicName", new CResourcePropertiesTranslatorInfo("TopicName", "TopicName", CResourcePropertiesTranslatorInfo.DEFAULT_PROPERTY_TRANSLATOR));
            L.Add(RTT.ResourceTypeName, RTT);

            File.WriteAllText("ResourceTranslatorInfos.json", JsonConvert.SerializeObject(L));



            var K = CParameterTranslatorInfo.AllParameterTranslators;
            CParameterTranslatorInfo PTI;

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "InstanceType";
            PTI.AzureParamName = "InstanceType";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "KeyPair";
            PTI.AzureParamName = "Keypair???";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "SecurityGroup";
            PTI.AzureParamName = "SecurityGroup";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "BucketName";
            PTI.AzureParamName = "BucketName";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "ConfigFile";
            PTI.AzureParamName = "ConfigFile";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "AmazonMachineImage";
            PTI.AzureParamName = "TargetImage";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            PTI = new CParameterTranslatorInfo();
            PTI.AWSParamName = "UserData";
            PTI.AzureParamName = "UserData";
            PTI.ParamTranslatorClass = "CDefaultParameterTranslator";
            K.Add(PTI.AWSParamName, PTI);

            File.WriteAllText("ParamTranslatorInfos.json", JsonConvert.SerializeObject(K));
        } //Createinfo
    }// program


    public class CloudFormation
    {
        private string templateUrl;
        awsObject awsObj = new awsObject();

        public CloudFormation(string pathToTemplate)
        {
            awsObj = new awsObject();
            this.templateUrl = pathToTemplate;
        }

        public awsObject awsObject
        {
            get
            {
                return awsObj;
            }
        }

        public void ParseTemplate()
        {
            List<IAWSParameter> awsParameters = new List<IAWSParameter>();
            List<awsResource> awsResources = new List<awsResource>();
            Dictionary<string, string> currentReferences = new Dictionary<string, string>();
            JObject jsonData;
            var amznConfig = new AmazonCloudFormationConfig();
            amznConfig.RegionEndpoint = RegionEndpoint.USWest1;
            AmazonCloudFormationClient client = new AmazonCloudFormationClient("AKIAJG44CNEV3UUDBPTQ", "BpxbaH8Q9rXyczKCPNxE47SO/+285mTddmlD/fT0", amznConfig);
            var response = client.GetTemplateSummary(new GetTemplateSummaryRequest
            {
                TemplateURL = this.templateUrl
            });

            using (WebClient wc = new WebClient())
            {
                var data = wc.DownloadData(this.templateUrl);
                string byteData = Encoding.UTF8.GetString(data);
                jsonData = JObject.Parse(byteData);
            }

            // Get as much as possible from the AWS obj for Parameters
            // TODO: The response obj is missing some of the attributes on each secion. Must look up object in JSON manually and add. 
            foreach (var param in response.Parameters)
            {
                IAWSParameter tempParam = new awsParameter();
                tempParam.Name = param.ParameterKey;
                foreach (JProperty para in jsonData["Parameters"][param.ParameterKey])
                {
                    PropertyInfo propertyInfo = tempParam.GetType().GetProperty(para.Name);
                    if (propertyInfo != null)
                    {
                        if (propertyInfo.PropertyType.Namespace == "System.Collections.Generic")
                        {
                            var temp = para.Value.ToObject(propertyInfo.PropertyType);
                            propertyInfo.SetValue(tempParam, Convert.ChangeType(temp, propertyInfo.PropertyType), null);
                        }
                        else
                        {
                            string[] typeArray = para.Value.ToString().Split(new[] { "::" }, StringSplitOptions.None);
                            string type = typeArray[typeArray.Length - 1];
                            propertyInfo.SetValue(tempParam, Convert.ChangeType(type, propertyInfo.PropertyType), null);
                        }
                    }
                }


                if (tempParam.Name != null && tempParam.Type != null)
                    currentReferences.Add(tempParam.Name, tempParam.Type);

                awsParameters.Add(tempParam);
            }

            bool firstRun = true;
            List<string> mustRevisit = new List<string>();
            while (firstRun == true || mustRevisit.Count > 0)
            {
                firstRun = false;

                foreach (JProperty child in jsonData["Resources"].Children())
                {
                    string[] tempNameArray = child.Path.Split('.');
                    string resName = tempNameArray[tempNameArray.Length - 1];
                    string[] resTypeArray = jsonData["Resources"][resName]["Type"].ToString().Split(new[] { "::" }, StringSplitOptions.None);
                    string resType = resTypeArray[resTypeArray.Length - 1];
                    if (resType == "Instance")
                    {
                        Instance instance = new Instance(resName);
                    }
                    else if (resType == "EIP")
                    {
                        EIP eip = new EIP(resName);
                        awsResources.Add(eip);
                    }
                    else if (resType == "SecurityGroup")
                    {
                        bool unInstantiatedRef = false;
                        Dictionary<string, string> properties = new Dictionary<string, string>();
                        string[] securityGroupIngressData = cleanProperty(jsonData["Resources"]
                                                                                [resName]
                                                                                ["Properties"]
                                                                                ["SecurityGroupIngress"]
                                                                                .ToString());

                        foreach (var prop in securityGroupIngressData)
                        {
                            string[] propKeyValue = prop.Split(':');
                            // Check for reference
                            if (propKeyValue[1].Contains("&"))
                            {
                                if (!currentReferences.ContainsKey(propKeyValue[1].Substring(1)))
                                {
                                    mustRevisit.Add(resType);
                                    unInstantiatedRef = true;
                                    continue;
                                }
                            }
                            // add it to dictionary
                            properties.Add(propKeyValue[0], propKeyValue[1]);
                        }

                        if (unInstantiatedRef == false)
                        {
                            var securityGroupIngress = new SecurityGroupIngress();
                            foreach (var prop in properties)
                            {
                                PropertyInfo propertyInfo = securityGroupIngress.GetType().GetProperty(prop.Key);
                                if (propertyInfo != null)
                                {
                                    propertyInfo.SetValue(securityGroupIngress, Convert.ChangeType(prop.Value, propertyInfo.PropertyType), null);
                                }

                            }

                            SecurityGroupProperties securityGroupProperties = new SecurityGroupProperties();
                            securityGroupProperties.GroupDescription = jsonData["Resources"]
                                                                                [resName]
                                                                                ["Properties"]
                                                                                ["GroupDescription"].ToString();
                            securityGroupProperties.SecurityGroupIngress = securityGroupIngress;
                            var securityGroup = new SecurityGroup(resName);
                            securityGroup.Name = resName;
                            securityGroup.Property = securityGroupProperties;
                            awsResources.Add(securityGroup);
                        }

                    }
                    else if (resType == "EIPAssociation")
                    {

                    }
                }
            }

            awsObj.Version = response.Version;
            awsObj.Description = response.Description;
            awsObj.Parameters = awsParameters;
            awsObj.Resources = awsResources;
        }

        static string[] cleanProperty(string str)
        {
            string tempStr = str.Replace("[", String.Empty)
                        .Replace("]", String.Empty)
                        .Replace("{", String.Empty)
                        .Replace("}", String.Empty)
                        .Replace("\"", String.Empty)
                        .Replace(" ", String.Empty)
                        .Replace("\n", String.Empty)
                        .Replace("\r", String.Empty);
            tempStr = tempStr.Replace("Ref:", "&");

            return tempStr.Split(',');
        }

    }


}
